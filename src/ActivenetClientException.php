<?php

namespace Drupal\openyhnl_activenet;

/**
 * Class ActivenetClientException.
 *
 * @package Drupal\openyhnl_activenet
 */
class ActivenetClientException extends \Exception
{
}
