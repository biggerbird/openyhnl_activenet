<?php

namespace Drupal\openyhnl_activenet;

/**
 * Interface ActivenetClientFactoryInterface.
 *
 * @package Drupal\openyhnl_activenet
 */
interface ActivenetClientFactoryInterface
{

  /**
   * Returns Activenet client.
   *
   * @return \GuzzleHttp\Client
   *   The http client.
   */
  public function get();
}
