<?php

namespace Drupal\openyhnl_activenet;

use GuzzleHttp\Client;

/**
 * Class ActivenetClient.
 *
 * @package Drupal\openyhnl_activenet
 *
 * @method mixed getCenters(array $args)
 * @method mixed getSites(array $args)
 * @method mixed getActivities(array $args)
 * @method mixed getActivityTypes(array $args)
 * @method mixed getActivityOtherCategories(array $args)
 * @method mixed getFlexRegPrograms(array $args)
 * @method mixed getFlexRegProgramTypes(array $args)
 * @method mixed getMembershipPackages(array $args)
 * @method mixed getMembershipCategories(array $args)
 * @method mixed getActivityDetail(int $id)
 */
class ActivenetClient extends Client implements ActivenetClientInterface
{

  /**
   * Settings
   *
   * @var array of settings from config
   */
  protected $api_settings;

  /**
   * ActivenetClient constructor
   * @param array $api_settings
   *   The api config settings.
   */

  public function setApi(array $api_settings)
  {
    $this->api_settings = $api_settings;
  }



  /**
   * Wrapper for 'request' method.
   *
   * @param string $method
   *   HTTP Method.
   * @param string $uri
   *   ActiveNet URI.
   * @param array $parameters
   *   Arguments.
   *
   * @return mixed
   *   Data from ActiveNet.
   *
   * @throws \Drupal\openyhnl_activenet\ActivenetClientException
   */
  private function makeRequest($method, $uri, array $parameters = [])
  {
    try {
      $response = $this->request($method, $uri, $parameters);

      if (200 != $response->getStatusCode()) {
        throw new ActivenetClientException(sprintf('Got non 200 response code for the uri %s.', $uri));
      }

      if (!$body = $response->getBody()) {
        throw new ActivenetClientException(sprintf('Failed to get response body for the uri %s.', $uri));
      }

      if (!$contents = $body->getContents()) {
        throw new ActivenetClientException(sprintf('Failed to get body contents for the uri: %s.', $uri));
      }

      $object = json_decode($contents);

      // Check if object contains data.
      if (isset($object->body)) {
        return $object->body;
      }

      throw new ActivenetClientException(sprintf('Got unknown body name for method %s.', $method));
    } catch (\Exception $e) {
      throw new ActivenetClientException(sprintf('Failed to make a request for uri %s with message %s.', $uri, $e->getMessage()));
    }
  }

  /**
   * Magic call method.
   *
   * @param string $method
   *   Method.
   * @param mixed $args
   *   Arguments.
   *
   * @return mixed
   *   Data.
   *
   * @throws ActivenetClientException.
   */
  public function __call($method, $args)
  {
    if (!$this->api_settings) {
      throw new ActivenetClientException(sprintf('Please inject api settings using "$this->setAPI($api_settings)".'));
    }

    $base_uri = $this->api_settings['base_uri'];

    switch ($method) {
      case 'makeRequest':
        throw new ActivenetClientException(sprintf('Please, extend Activenet client!', $method));

      case 'getCenters':
        return $this->makeRequest('get', $base_uri . 'centers' . $this->makeSuffix($args));

      case 'getSites':
        return $this->makeRequest('get', $base_uri . 'sites' . $this->makeSuffix($args));

      case 'getActivities':
        $category_id = array_shift($args);
        if ($category_id) $args = $this->addQueryParam($args, 'category_id', $category_id);
        $args = $this->addQueryParam($args, 'activity_status_id', '0');
        return $this->makeRequest('get', $base_uri . 'activities' . $this->makeSuffix($args));

      case 'getActivityDetail':
        $id = array_shift($args);
        return $this->makeRequest('get', $base_uri . 'activities/' . $id . $this->makeSuffix($args));

      case 'getActivityTypes':
        return $this->makeRequest('get', $base_uri . 'activitytypes' . $this->makeSuffix($args));

      case 'getActivityCategories':
        return $this->makeRequest('get', $base_uri . 'activitycategories' . $this->makeSuffix($args));

      case 'getActivityOtherCategories':
        return $this->makeRequest('get', $base_uri . 'activityothercategories' . $this->makeSuffix($args));

      case 'getFlexRegPrograms':
        return $this->makeRequest('get', $base_uri . 'flexregprograms' . $this->makeSuffix($args));

      case 'getFlexRegProgramTypes':
        return $this->makeRequest('get', $base_uri . 'flexregprogramtypes' . $this->makeSuffix($args));

      case 'getMembershipCategories':
        return $this->makeRequest('get', $base_uri . 'membershippackagecategories' . $this->makeSuffix($args));

      case 'getMembershipPackages':
        $category_id = array_shift($args);
        if ($category_id) $args = $this->addQueryParam($args, 'package_category_id', $category_id);
        return $this->makeRequest('get', $base_uri . 'membershippackages' . $this->makeSuffix($args));

      case 'getMembershipPackageFees':
        $id = array_shift($args);
        return $this->makeRequest('get', $base_uri . 'membershippackages/'  . $id . '/fees' . $this->makeSuffix($args));
    }

    throw new ActivenetClientException(sprintf('Method %s not implemented yet.', $method));
  }

  function addQueryParam($args, $name, $value)
  {
    if (empty($args)) $args[] = array();
    $args[0][$name] = $value;
    return $args;
  }

  function makeSuffix($args)
  {
    $api_key = $this->api_settings['api_key'];
    $secret = $this->api_settings['api_secret'];

    // Prepare suffix for the endpoint.
    $args = $this->addQueryParam($args, 'api_key', $api_key);
    $args = $this->addQueryParam($args, 'sig', $this->signature($api_key, $secret));
    // $args = $this->addQueryParam($args, 'total_records_per_page', 200);

    return '?' . http_build_query($args[0]);
  }

  function signature($api_key, $secret)
  {
    $timestamp = time();
    return hash('sha256', $api_key . $secret . $timestamp);
  }
}
